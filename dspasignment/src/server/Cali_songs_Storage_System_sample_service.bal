import ballerina/crypto;
import ballerina/grpc;
import ballerina/io;
import ballerina/mongodb;

listener grpc:Listener ep = new (9090);

mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 27017,
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};

mongodb:Client server = checkpanic new (mongoConfig);
mongodb:Database mongoDatabase = checkpanic server->getDatabase("CalliRecords");
mongodb:Collection records = checkpanic mongoDatabase->getCollection("records");

service Cali_songs_Storage_System on ep {
    //resource method to write Record and store it
    resource function writeRecord(grpc:Caller caller, Record value) {
        // Implementation goes here.
        // --------------------- hashing function -------------------****
        byte[] recordKey = crypto:hashSha256(value.toString().toBytes());
        // --------------------- hashing ending----------------------****

        //convert record into a json
        map<json>|error recordJson = map<json>.constructFrom(value);
        //check if the above convertion- is error
        if (recordJson is error) {
            io:println("Error");
            //sending error if failed to convert it into json 
            grpc:Error? err = caller->sendError(123,"Failed Covert Record into a json");
        } else {
            //inserting record in json object
            recordJson["RecordKey"] = recordKey.toBase16();
            //inserting data into a database..
            checkpanic records->insert(recordJson);
            // You should return a response showing that the record has been saved
            grpc:Error? result = caller->send({RecordKey: recordKey.toBase16(), RecordVersion: 1});
            if (result is grpc:Error) {
                result = caller->sendError(404, "Page Not Found");
            }
            result = caller->complete();
        }

    // You should return a recordKeyVersion
    }

    //resource function to update a record
    resource function updatingExistingRecord(grpc:Caller caller, updateRecord value) {
        //checking if the record exist or not in the database
        map<json>[] return_from_db = checkpanic records->find({"RecordKey": value.RecordKey, "RecordVersion": value.RecordVersion});

        if (return_from_db.length() > 0) {
            //array to store all version comming from the database......
            int[] storedversions = [];
            //getting update record from client and converting into a json file
            map<json>|error updatingrec = map<json>.constructFrom(value.recordCopy);
            //getting all records relating to that key
            map<json>[] all_records = checkpanic records->find({"RecordKey": value.RecordKey});
            if (updatingrec is error) {
                io:println("Error");
                grpc:Error? err = caller->sendError(123,"Cannot Conert Record into Json");
            } else {
                //inserting record in json object
                foreach var vers in all_records {
                    storedversions.push(<int>vers.RecordVersion);
                }
                //sorting All versions in descending order
                int[] sortedVersions = storedversions.sort(function(int a, int b) returns (int) {
                    return b - a;
                });

                //new generated version
                updatingrec["RecordVersion"] = sortedVersions[0] + 1;
                updatingrec["RecordKey"] = value.RecordKey;

                //inserting an updated record into the database.......
                checkpanic records->insert(updatingrec);
                // You should return a Confirmation that the data is updated....
                grpc:Error? result = caller->send({RecordKey: value.RecordKey, RecordVersion: sortedVersions[0] + 1});
                if (result is grpc:Error) {
                    result = caller->sendError(404, "Page Not Found");
                }
                result = caller->complete();
            }
        } else {
            grpc:Error? err = caller->sendError(502, "Record does not exist!");
        }
    // You should return a recordKeyVersion
    }
    //resource method to Read record with key and get latest version
    resource function readRecordWithKey(grpc:Caller caller, recordKey value) {
        //array that will receive all records related from the database.....
        map<json>[] all_rec = checkpanic records->find(({"RecordKey": value.RecordKey}),({"RecordVersion": -1}));

        if (all_rec.length() > 0) {
             //int[] storedversions = [];
            //inserting record in json object
            // foreach var vers in all_rec {
            //     storedversions.push(<int>vers.RecordVersion);
            // }
            // //sorting All versions in descending order
            // int[] sortedVer = storedversions.sort(function(int a, int b) returns (int) {
            //     return b - a;
            // });
            // map<json>[] reqRecord = checkpanic records->find({"RecordKey": value.RecordKey, "RecordVersion": sortedVer[0]});
            //grpc:Error? result = caller->send(reqRecord[0]);
            grpc:Error? result = caller->send(all_rec[0]);
            if (result is grpc:Error) {
                result = caller->sendError(502, "Error!!");
            }
            result = caller->complete();
        } else {
            grpc:Error? res = caller->sendError(502, "Record does not exist!");
        }
        // You should return a Record
    }

    //resource method to get/read record with key and version and get coresponding Record
    resource function readRecordWithKeyVer(grpc:Caller caller, recordKeyVersion value) {
        // Implementation goes here.
       // map<json> jsonfilter = {"RecordKey": value.RecordKey, "RecordVersion": value.RecordVersion};
        //json that contain all data coming from the database
        map<json>[] return_from_db = checkpanic records->find({"RecordKey": value.RecordKey, "RecordVersion": value.RecordVersion});

        //checking if the array has values
        if (return_from_db.length() > 0) {

                grpc:Error? result = caller->send(return_from_db[0]);

                if (result is grpc:Error) {
                    result = caller->sendError(404, "Page Not Found");
                }
                result = caller->complete();
        } else {
            grpc:Error? err = caller->sendError(502, "Record does not exist!");
        }
    // You should return a Record
    }

    //resource method to read records with Criterion and get all records that correspond
    @grpc:ResourceConfig {streaming: true}
    resource function readRecordWithCriterion(grpc:Caller caller, readWithCriterion value) {
        map<json>[] allrecords = checkpanic records->find(());       
        io:println(value);
        io:println("-------------------------------------------\n\n");
        //io:println(allrecords.toJsonString());
        // foreach var item in allrecords {
        //     io:println(item.toJsonString());
        //     io:print("\n\n");
        // }
        // map<json>[]  hell =  checkpanic records->find({string `$text:{$search: value.artistname}`);  

        //   var hello =  allrecords.filter(function(any rec) returns boolean{
        //         return (rec.name===value.artistname || rec.artist)? true:false;
        //        // return true;
        //     });

        map<json>[] artists = [];

        foreach var item in allrecords {
            if (item.band === value.bandName) {
                io:println(item.toJsonString(),"\n");
                grpc:Error? result = caller->send(item);
                if (result is grpc:Error){
                      result = caller->sendError(234,"Error");
                }

                //artists.push(map<json> item.artists);

                //result = caller->complete();
            }
        }
            // method to do the search
           crit(value);
    }
}

function crit(readWithCriterion value) {
    //map<json> bandFilter = {"$text":{"$search":value.bandName}};
    if(value.bandName !==""){
        map<json> bandFilter = {"$text":{"$search":value.bandName}};
        map<json>[] allrecords = checkpanic records->find((bandFilter));
        io:println(allrecords,"\n\n");
    }else if(value.artistname !==""){
        map<json> artistFilter = {"$text":{"$search":value.artistname}};
        map<json>[] allrecords = checkpanic records->find((artistFilter));
        io:println(allrecords,"\n\n");
    }else if(value.songtitle !==""){
        map<json> songFilter = {"$text":{"$search":value.artistname}};
        map<json>[] allrecords = checkpanic records->find(( songFilter));
        io:println(allrecords,"\n\n");
    }

    if(value.bandName!=="" && value.artistname!=="" && value.songtitle!==""){

        map<json> jsonfilter = {
            "band":value.bandName,
            "artists":[{
               "name":value.artistname 
            }],
            "songs":[{
                "title":value.songtitle
            }]
        };

        map<json>[] jsonret = checkpanic records->find(jsonfilter);
        io:println(jsonret);
        // map<json>[] jsn = checkpanic records->find(());
        // io:println();
    }
}

public type Record record {|
    string RecordKey = "";
    int RecordVersion = 0;
    string name = "";
    string date = "";
    Artist[] artists = [];
    string band = "";
    Song[] songs = [];

|};

public type Artist record {|
    string name = "";
    Member? member = ();

|};

public type Member "YES"|"NO";
public const Member MEMBER_YES = "YES";
const Member MEMBER_NO = "NO";


public type Song record {|
    string title = "";
    string genre = "";
    string platform = "";

|};


public type recordKey record {|
    string RecordKey = "";

|};

public type recordKeyVersion record {|
    string RecordKey = "";
    int RecordVersion = 0;

|};

public type updateRecord record {|
    string RecordKey = "";
    int RecordVersion = 0;
    Record? recordCopy = ();

|};

public type readWithCriterion record {|
    string songtitle = "";
    string artistname = "";
    string bandName = "";

|};



const string ROOT_DESCRIPTOR = "0A1963616C695F73746F726167655F73797374656D2E70726F746F228E030A065265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12120A046E616D6518032001280952046E616D6512120A046461746518042001280952046461746512280A076172746973747318052003280B320E2E5265636F72642E41727469737452076172746973747312120A0462616E64180620012809520462616E6412220A05736F6E677318072003280B320C2E5265636F72642E536F6E675205736F6E67731A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F10011A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22290A097265636F72644B6579121C0A095265636F72644B657918012001280952095265636F72644B657922560A107265636F72644B657956657273696F6E121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E227B0A0C7570646174655265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12270A0A7265636F7264436F707918032001280B32072E5265636F7264520A7265636F7264436F7079226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65329A020A1943616C695F736F6E67735F53746F726167655F53797374656D12290A0B77726974655265636F726412072E5265636F72641A112E7265636F72644B657956657273696F6E123A0A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A112E7265636F72644B657956657273696F6E12280A11726561645265636F7264576974684B6579120A2E7265636F72644B65791A072E5265636F726412320A14726561645265636F7264576974684B657956657212112E7265636F72644B657956657273696F6E1A072E5265636F726412380A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A072E5265636F72643001620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "cali_storage_system.proto": "0A1963616C695F73746F726167655F73797374656D2E70726F746F228E030A065265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12120A046E616D6518032001280952046E616D6512120A046461746518042001280952046461746512280A076172746973747318052003280B320E2E5265636F72642E41727469737452076172746973747312120A0462616E64180620012809520462616E6412220A05736F6E677318072003280B320C2E5265636F72642E536F6E675205736F6E67731A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F10011A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22290A097265636F72644B6579121C0A095265636F72644B657918012001280952095265636F72644B657922560A107265636F72644B657956657273696F6E121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E227B0A0C7570646174655265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12270A0A7265636F7264436F707918032001280B32072E5265636F7264520A7265636F7264436F7079226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65329A020A1943616C695F736F6E67735F53746F726167655F53797374656D12290A0B77726974655265636F726412072E5265636F72641A112E7265636F72644B657956657273696F6E123A0A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A112E7265636F72644B657956657273696F6E12280A11726561645265636F7264576974684B6579120A2E7265636F72644B65791A072E5265636F726412320A14726561645265636F7264576974684B657956657212112E7265636F72644B657956657273696F6E1A072E5265636F726412380A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A072E5265636F72643001620670726F746F33"

    };
}

