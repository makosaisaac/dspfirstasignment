import ballerina/grpc;
import ballerina/io;
import ballerina/lang.'int;

public function main (string... args) {
        //unary blocking client
    Cali_songs_Storage_SystemBlockingClient blockingEp = new("http://localhost:9090");
        //unary non-blocking client
    Cali_songs_Storage_SystemClient nonblockingEp = new("http://localhost:9090");


    io:println("******************** CALI 🎸🎻🪕*********************");
    io:println("----------------------------------------------------");
    io:println("1.  Create/Write Record");
    io:println("2.  Update Record");
    io:println("3.  Read Record With Key");
    io:println("4.  Read Record With Key and Version");
    io:println("5.  Read Record With Criterion |artist name|song title|band");
    io:println("----------------------------------------------------");
    string choose = io:readln("Select Option: ");

    if (choose === "1") {
            createRecord(blockingEp);
    } else if (choose === "2") {
           updateRec(blockingEp);
    } else if (choose === "3") {
           ReadRec(blockingEp);
    } else if (choose === "4") {
           ReadRecKeyV(blockingEp);
    } else if (choose === "5") {
            ReadWithCriterion(nonblockingEp);
    } else {
        io:println("Wrong Option please try again!!");
        main();
    }
}

//method to create a record
function createRecord(Cali_songs_Storage_SystemBlockingClient bc) {
    Record rec = {};
    io:println("\n---------------Create a record--------------");
    string name = io:readln("Enter Record Name: ");
    string date = io:readln("Enter Release Date: ");
    string band = io:readln("Enter Band Name: ");

    rec.name = name;
    rec.date = date;
    rec.band = band;
    rec.RecordVersion = 1;

    //enter number of artist
    string anumber = io:readln("Enter Number of Artist: ");
    int|error count = 'int:fromString(anumber);
    if (count is error) {
    //print the error
    } else {
        foreach int i in 1 ... count {
            io:println("Artist Number: ", i);
            string artname = io:readln("Enter Artist Name: ");
            string member = io:readln("Member? YES|NO : ");
            if (member.toLowerAscii() === "yes") {
                rec.artists.push({name: artname, member: MEMBER_YES});
            } else {
                rec.artists.push({name: artname, member: MEMBER_NO});
            }

        }
    }

    //putting a conut on how many songs in the record or albulm
    string songc = io:readln("Enter Number of Songs: ");
    int|error sc = 'int:fromString(songc);

    if (sc is error) {

    } else {
        foreach int i in 1 ... sc {
            io:println("Song Number ", i);
            string title = io:readln("Enter Song Title: ");
            string genre = io:readln("Enter Song Genre: ");
            string plat = io:readln("Enter Song Platform: ");

            rec.songs.push({title: title, genre: genre, platform: plat});

        }
    }

    var result = bc->writeRecord(rec);
    if (result is grpc:Error) {
        io:println(result);
        io:println("------------------------------------------");
        string op = io:readln("1. Create New Record: \n2. Main: \n");
        var c = (op === "1") ? createRecord(bc) : main();
    } else {
        io:println("--------------------Response After Creating a Record----------------------");
        io:println(result);
        io:println("--------------------Ending Of Record Creation-----------------------------");
        string op = io:readln("1. Create New Record: \n2. Main: \n");
        var c = (op === "1") ? createRecord(bc) : main();
    }

}

//method to update record
function updateRec(Cali_songs_Storage_SystemBlockingClient bc) {
    Record rec = {};
    io:println("\n---------------Update a New record--------------");
    string recordkey = io:readln("Enter Record Key: ");
    string ver = io:readln("Enter Record Version: ");
    int|error recordver = 'int:fromString(ver);

    io:println("-------------New Record---------------");
    string name = io:readln("Enter Record Name: ");
    string date = io:readln("Enter Release Date: ");
    string band = io:readln("Enter Band Name: ");

    rec.name = name;
    rec.date = date;
    rec.band = band;
    rec.RecordVersion = 1;

    //enter number of artist
    string anumber = io:readln("Enter Number of Artist: ");
    int|error count = 'int:fromString(anumber);

    foreach int i in 1 ... <int>count {
        io:println("Artist Number: ", i);
        string artname = io:readln("Enter Artist Name: ");
        string member = io:readln("Member? YES|NO : ");
        if (member.toLowerAscii() === "yes") {
            rec.artists.push({name: artname, member: MEMBER_YES});
        } else {
            rec.artists.push({name: artname, member: MEMBER_NO});
        }

    }


    //putting a conut on how many songs in the record or albulm
    string songc = io:readln("Enter Number of Songs: ");
    int|error sc = 'int:fromString(songc);

    foreach int i in 1 ... <int>sc {
        io:println("Song Number ", i);
        string title = io:readln("Enter Song Title: ");
        string genre = io:readln("Enter Song Genre: ");
        string plat = io:readln("Enter Song Platform: ");

        rec.songs.push({title: title, genre: genre, platform: plat});

    }




    var result = bc->updatingExistingRecord({RecordKey: recordkey, RecordVersion: <int>recordver, recordCopy: rec});
    if (result is grpc:Error) {
        io:println("--------------------Error-----------------------");
        io:println(result);
        io:println("------------------------------------------------");
        string op = io:readln("1. Update Record: \n2. Main: \n");
        var c = (op === "1") ? updateRec(bc) : main();
    } else {
        io:println("-------------------Response After Updating Record---------------------");
        io:println(result);
        io:println("-----------------------Ending Of Record Update------------------------");
        string op = io:readln("1. Update Record: \n2. Main: \n");
        var c = (op === "1") ? updateRec(bc) : main();
    }

}

//method to read record with a key
function ReadRec(Cali_songs_Storage_SystemBlockingClient nb) {
    string recordkey = io:readln("Enter Record Key: ");

    var result = nb->readRecordWithKey({RecordKey: <string>recordkey});
    if (result is grpc:Error) {
        io:println("---------------------Error-------------------------");
        io:println(result);
        io:println("---------------------------------------------------");
        string op = io:readln("1. Read Record with a Key: \n2. Main: \n");
        var c = (op === "1") ? ReadRec(nb) : main();
    } else {
        io:println("--------|**Response After Reading  Record With A Key Only***|-----\n");
        io:println(result);
        io:println("\n------------------------------------------------------------------");
        string op = io:readln("1. Read Record with a Key: \n2. Main: \n");
        var c = (op === "1") ? ReadRec(nb) : main();
    }
}

//method to read record with a key and version
function ReadRecKeyV(Cali_songs_Storage_SystemBlockingClient nb) {
    string recordkey = io:readln("Enter Record Key: ");
    string ver = io:readln("Enter Record Version: ");
    int|error recordver = 'int:fromString(ver);

    var result = nb->readRecordWithKeyVer({RecordKey: recordkey, RecordVersion: <int>recordver});
    if (result is grpc:Error) {
        io:println("------------------------Error---------------------------");
        io:println(result);
        io:println("--------------------------------------------------------");
        string op = io:readln("1. Read Record with a Key & Version: \n2. Main: \n");
        var c = (op === "1") ? ReadRecKeyV(nb) : main();
    } else {
        io:println("------------|**** Response After Reading  Record With A Key Only***|------------ \n");

        io:println(result);

        io:println("\n---------------------------------------------------------------------------------");
        string op = io:readln("1. Read Record with a Key & Version: \n2. Main: \n");
        var c = (op === "1") ? ReadRecKeyV(nb) : main();
    }
}

//method to read with a Criterion
function ReadWithCriterion(Cali_songs_Storage_SystemClient nb) {
    string artistname = io:readln("Enter Artist name: ");
    string bandname = io:readln("Enter Band: ");
    string songtitle = io:readln("Enter Song Title: ");

    var result = nb->readRecordWithCriterion({artistname: artistname,bandName: bandname,songtitle: songtitle},GetRecord);

    string op = io:readln("1. Read Record with Criterion Artist Name|Band|Song Title: \n2. Main: \n");
    var c = (op === "1") ? ReadWithCriterion(nb) : main();
}

//service listener to responds for reading records. 
service GetRecord = service {
        function onError(error err) {
              io:print("Error ", err.detail());
       }

       function onMessage(Record rec){
              io:println("Record Name: ",rec.name);
              io:println("Release Date: ",rec.date);
              io:println("Band: ",rec.band);
              io:println("----------Artists🎵🎵🎤-----------");
              io:println("Artist Name\t\tMember");
              foreach var artist in rec.artists {
                     io:println(artist.name,"\t\t",artist.member);
              }
              io:println("🎵🎧🎧🎧🎧**Songs**🎧🎧🎧🎧🎵");
              io:println("Title\t\tGenre\t\tPlatform");
              foreach var song in rec.songs {
                     io:println(song.title,"\t\t",song.genre,"\t\t",song.platform);
              }
       }

       function onComplete() {
              io:print("Completed!!!");
       }
};

