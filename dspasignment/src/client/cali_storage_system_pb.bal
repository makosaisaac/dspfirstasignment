import ballerina/grpc;

public type Cali_songs_Storage_SystemBlockingClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(Record req, grpc:Headers? headers = ()) returns ([recordKeyVersion, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali_songs_Storage_System/writeRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<recordKeyVersion>result, resHeaders];
        
    }

    public remote function updatingExistingRecord(updateRecord req, grpc:Headers? headers = ()) returns ([recordKeyVersion, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali_songs_Storage_System/updatingExistingRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<recordKeyVersion>result, resHeaders];
        
    }

    public remote function readRecordWithKey(recordKey req, grpc:Headers? headers = ()) returns ([Record, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali_songs_Storage_System/readRecordWithKey", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<Record>result, resHeaders];
        
    }

    public remote function readRecordWithKeyVer(recordKeyVersion req, grpc:Headers? headers = ()) returns ([Record, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali_songs_Storage_System/readRecordWithKeyVer", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<Record>result, resHeaders];
        
    }

    public remote function readRecordWithCriterion(readWithCriterion req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali_songs_Storage_System/readRecordWithCriterion", req, msgListener, headers);
    }

};

public type Cali_songs_Storage_SystemClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "non-blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(Record req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali_songs_Storage_System/writeRecord", req, msgListener, headers);
    }

    public remote function updatingExistingRecord(updateRecord req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali_songs_Storage_System/updatingExistingRecord", req, msgListener, headers);
    }

    public remote function readRecordWithKey(recordKey req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali_songs_Storage_System/readRecordWithKey", req, msgListener, headers);
    }

    public remote function readRecordWithKeyVer(recordKeyVersion req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali_songs_Storage_System/readRecordWithKeyVer", req, msgListener, headers);
    }

    public remote function readRecordWithCriterion(readWithCriterion req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali_songs_Storage_System/readRecordWithCriterion", req, msgListener, headers);
    }

};

public type Record record {|
    string RecordKey = "";
    int RecordVersion = 0;
    string name = "";
    string date = "";
    Artist[] artists = [];
    string band = "";
    Song[] songs = [];
    
|};

public type Artist record {|
    string name = "";
    Member? member = ();
    
|};

public type Member "YES"|"NO";
public const Member MEMBER_YES = "YES";
const Member MEMBER_NO = "NO";


public type Song record {|
    string title = "";
    string genre = "";
    string platform = "";
    
|};



public type recordKey record {|
    string RecordKey = "";
    
|};


public type recordKeyVersion record {|
    string RecordKey = "";
    int RecordVersion = 0;
    
|};


public type updateRecord record {|
    string RecordKey = "";
    int RecordVersion = 0;
    Record? recordCopy = ();
    
|};


public type readWithCriterion record {|
    string songtitle = "";
    string artistname = "";
    string bandName = "";
    
|};



const string ROOT_DESCRIPTOR = "0A1963616C695F73746F726167655F73797374656D2E70726F746F228E030A065265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12120A046E616D6518032001280952046E616D6512120A046461746518042001280952046461746512280A076172746973747318052003280B320E2E5265636F72642E41727469737452076172746973747312120A0462616E64180620012809520462616E6412220A05736F6E677318072003280B320C2E5265636F72642E536F6E675205736F6E67731A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F10011A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22290A097265636F72644B6579121C0A095265636F72644B657918012001280952095265636F72644B657922560A107265636F72644B657956657273696F6E121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E227B0A0C7570646174655265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12270A0A7265636F7264436F707918032001280B32072E5265636F7264520A7265636F7264436F7079226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65329A020A1943616C695F736F6E67735F53746F726167655F53797374656D12290A0B77726974655265636F726412072E5265636F72641A112E7265636F72644B657956657273696F6E123A0A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A112E7265636F72644B657956657273696F6E12280A11726561645265636F7264576974684B6579120A2E7265636F72644B65791A072E5265636F726412320A14726561645265636F7264576974684B657956657212112E7265636F72644B657956657273696F6E1A072E5265636F726412380A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A072E5265636F72643001620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "cali_storage_system.proto":"0A1963616C695F73746F726167655F73797374656D2E70726F746F228E030A065265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12120A046E616D6518032001280952046E616D6512120A046461746518042001280952046461746512280A076172746973747318052003280B320E2E5265636F72642E41727469737452076172746973747312120A0462616E64180620012809520462616E6412220A05736F6E677318072003280B320C2E5265636F72642E536F6E675205736F6E67731A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F10011A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22290A097265636F72644B6579121C0A095265636F72644B657918012001280952095265636F72644B657922560A107265636F72644B657956657273696F6E121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E227B0A0C7570646174655265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12270A0A7265636F7264436F707918032001280B32072E5265636F7264520A7265636F7264436F7079226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65329A020A1943616C695F736F6E67735F53746F726167655F53797374656D12290A0B77726974655265636F726412072E5265636F72641A112E7265636F72644B657956657273696F6E123A0A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A112E7265636F72644B657956657273696F6E12280A11726561645265636F7264576974684B6579120A2E7265636F72644B65791A072E5265636F726412320A14726561645265636F7264576974684B657956657212112E7265636F72644B657956657273696F6E1A072E5265636F726412380A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A072E5265636F72643001620670726F746F33"
        
    };
}

