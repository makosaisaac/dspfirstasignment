import ballerina/grpc;
import ballerina/io;
public function main (string... args) {

    songsStorageSystemBlockingClient blockingEp = new("http://localhost:9090");

    // var result = blockingEp->insertRec({
    //                                     "name":"isaac","date":"2020-11-29",
    //                                     "band":"Isaac Band","songs":[
    //                                         {"title":"Isaac Song","genre":"ROCK","platform":"I dont know"}
    //                                         ,{"title":"isabavsgs","genre":"amapiano","platform":"folk rock"}
    //                                         ],
    //                                         "artist":[
    //                                             {"name":"isaac","member":"YES"},{"name":"Taku","member":"NO"},
    //                                             {"name":"Engela","member":"NO"},
    //                                             {"name":"Kyle","member":"YES"},
    //                                             {"name":"Tin Tin","member":"YES"}
    //                                         ],
    //                                         "RecordVersion": 1
    //                                     });

    //    if(result is grpc:Error){
    //        io:println(result.reason());
    //    }else{
    //        io:println(result);
    //    }            

    // var res = blockingEp->updatingExistingRecord({
    //     RecordKey: "96941e303f8f49f9ea8fff2655f7d965",
    //     RecordVersion: 2,
    //     the_record: {
    //         "name":"isaac","date":"2020-11-29",
    //                     "band":"Isaac Band","songs":[
    //                                 {"title":"Isaac Song","genre":"ROCK","platform":"I dont know"}
    //                                 ,{"title":"isabavsgs","genre":"amapiano","platform":"folk rock"}
    //                                         ],
    //                                         "artist":[
    //                                             {"name":"isaac","member":"YES"},{"name":"Taku","member":"NO"},
    //                                             {"name":"Engela","member":"NO"},
    //                                             {"name":"Kyle","member":"YES"},
    //                                             {"name":"Tin Tin","member":"YES"}
    //                                         ],
    //                                         "RecordVersion": 1
    //     }
    // });                     

    //     if(res is grpc:Error){
    //        io:println(res.reason());
    //     }else{
    //        io:println(res);
    //     } 

    var res = blockingEp->readRecordWithKey({RecordKey: "96941e303f8f49f9ea8fff2655f7d965"});
     if(res is grpc:Error){
           io:println(res.reason());
    }else{
           io:println(res);
    } 

    // var res = blockingEp->readRecordWithKeyVer({RecordKey: "96941e303f8f49f9ea8fff2655f7d965",RecordVersion: 0});
    //  if(res is grpc:Error){
    //        io:println(res.reason());
    // }else{
    //        io:println(res);
    // } 
}


